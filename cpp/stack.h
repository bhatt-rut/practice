#ifndef INCLUDED_RLIB_STACK
#define INCLUDED_RLIB_STACK

namespace rlib {
// only supports integers.
class stack {
    private:
	struct stack_node {
	    int data;
	    stack_node *next;

	    stack_node(int x) : data(x), next(0) {}
	};

    private:
	stack_node *top;
	int d_size;
	void empty_check();

    public:
	stack();
	~stack();

	void push(int x);
	int pop();
	int peek();
	int size() { return d_size; }
};

} // close namespace rlib

#endif
