#include <stack.h>
#include <cassert>

using stack = rlib::stack;

void simple_test()
{
    stack s;
    s.push(15);
    int ret = s.pop();
    assert(ret == 15);
}

void bigger_test()
{
    stack s;
    for (int i = 0; i < 100; ++i) {
	s.push(i);
    }
    assert(100 == s.size());

    // remove the top item.
    int top_item = s.pop();
    assert(99 == top_item);
    top_item = s.peek();
    assert(98 == top_item);
    assert(99 == s.size());

    for (int i = 0; i < 10; ++i) {
	s.pop();
    }
}

int main()
{
    simple_test();
    bigger_test();
}
