#include <queue.h>
#include <cassert>
#include <stdexcept>

namespace rlib {

queue::queue() : d_size(0), d_first(0)
{
}

queue::~queue()
{
    if (0 == d_first) {
	return;
    }

    while (d_first) {
	queue_node * temp = d_first;
	d_first = d_first->next;
	delete temp;
	--d_size;
    }

    assert(0 == d_size);
}

void queue::push(int x)
{
    ++d_size;
    if (0 == d_first) {
	d_first = new queue_node(x);
	return;
    }

    queue_node * walker = d_first;
    while (walker->next != 0) {
	walker = walker->next;
    }
    walker->next = new queue_node(x);
}

int queue::pop()
{
    if (0 == d_first) {
	throw std::runtime_error("Empty queue.");
    }

    int value = d_first->data;
    queue_node * temp = d_first;
    d_first = d_first->next;
    --d_size;
    delete temp;
    return value;
}

} // close namespace rlib
