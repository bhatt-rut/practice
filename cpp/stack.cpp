#include <stack.h>
#include <stdexcept>
#include <iostream>
#include <cassert>

namespace rlib {

stack::stack() : top(0), d_size(0)
{
}

stack::~stack()
{
    int dealloc_count = 0;
    while (top && top->next) {
	stack_node *temp = top;
	top = top->next;
	delete temp;
	--d_size;
	++dealloc_count;
    }
    std::cout << "removed " << dealloc_count << " nodes from stack\n";
    assert(0 == d_size);
}

void stack::push(int x)
{
    stack_node *node = new stack_node(x);
    if (0 == top) {
	top = node;
    } else {
	stack_node *temp = top;
	top = node;
	top->next = temp;
    }
    ++d_size;
}

void stack::empty_check()
{
    if (0 == top) {
	throw std::runtime_error("empty stack.");
    }
}

int stack::pop()
{
    empty_check();

    stack_node *temp = top;
    top = top->next;
    int value = temp->data;
    delete temp;
    --d_size;
    return value;
}

int stack::peek()
{
    empty_check();
    return top->data;
}

}
