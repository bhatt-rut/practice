import sys
import itertools

"""The strategy for this problem is to search through the large problem space for
   few jamcoins. Since the problem space is so large and the numbers so big, it 
   is more efficient to spend little time on each number (by testing few divisors)
   on it. Tried the more brute force way and it was far too slow."""

def _is_prime_(num):
    """Returns True if the number is prime, False otherwise. (slow)"""
    if num % 2 == 0 or num % 3 == 0:
        return False

    for i in range(5, int(num ** 0.5) + 1, 2):
        if num % i == 0:
            return False
    return True

def _gen_coprimes_(c_sharp):
    """Generates coprimes for some co-efficient to the c#k + 1 equation"""
    # find all coprimes of c_sharp
    coprimes = list()
    for i in range(2, c_sharp):
        if _is_prime_(i) and c_sharp % i != 0:
            coprimes.append(i)
        
    return coprimes

def _factors_(num):
    factors = list()
    i = 2
    while True:
        if num % i == 0:
            factors.append(i)
            num /= i
            i = 2 # start again with 2
        else:
            i += 1

        if 1 == num:
            break

    return factors
    
g_step     = 200
# store the first 200 coprimes for quick lookups.
g_coprimes = _gen_coprimes_(g_step)
g_factors  = set(_factors_(g_step))

def fast_prime(num):
    if num % 2 == 0 or num % 3 == 0:
        return False
    
    if num in g_factors:
        return False

    for i in xrange(0, int(num ** 0.5) + 1, g_step):
        # look only at the coprimes
        for j in g_coprimes:
            if num % (i + j) == 0:
                return False

    return True

def get_divisor(num):
    if num % 2 == 0:
        return 2
    if num % 3 == 0:
        return 3

    for f in g_factors:
        if num % f == 0:
            return f

    for c in g_coprimes:
        if num % c == 0:
            return c

    return None

def gen_potential_jamcoins(length):
    for p in itertools.product('01', repeat=(length-2)):
        yield '1' + ''.join(p) + '1'

def solve(jamcoin_len, num_jamcoins):
    jamcoins = list()
    for pcoin in gen_potential_jamcoins(jamcoin_len):
        divs = list()
        ok = True
        # check divisors
        for base in range(2, 11):
            div = get_divisor(int(pcoin, base))
            if not div:
                ok = False
                break
            else:
                divs.append(div)

        if ok:
            jamcoins.append((pcoin, [str(d) for d in divs]))

        if num_jamcoins == len(jamcoins):
            break
        
    print "Case #1:"
    for coin, divs in jamcoins:
        print "{} {}".format(coin, ' '.join(divs))


l = sys.stdin.readlines()
relevant_line = l[1].strip()
(n, j) = relevant_line.split(' ')
jamcoin_len = int(n)
num_jamcoins = int(j)

jamcoins = solve(jamcoin_len, num_jamcoins)

