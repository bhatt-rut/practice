import sys

def swap(arr, i, j):
    t = arr[i]
    arr[i] = arr[j]
    arr[j] = t

def _permute_impl_(arr, l, h):

    if l == h:
        print ''.join(arr)
        return

    for j in range(l, h+1):
        swap(arr, j, l) # swap first item with all others in a loop
        _permute_impl_(arr, l+1, h)
        swap(arr, j, l)

def permute(arr):
    _permute_impl_(list(arr), 0, len(arr)-1)


def main():
    permute(sys.stdin.readlines()[0].strip())

if "__main__" == __name__:
    main()
