#ifndef INCLUDED_LRUCACHE
#define INCLUDED_LRUCACHE

#include <list>
#include <unordered_map>
#include <iostream>

namespace rlib {

template<typename KEY, typename VALUE, class Hash = std::hash<KEY>, class KeyEqual = std::equal_to<KEY>>
class LRUCache {
private:
    struct list_item {
	KEY key;
	VALUE value;

	list_item(const KEY &k, const VALUE &v) : key(k), value(v) {}
    };

    std::list<list_item> items_;

    typedef std::unordered_map<
	KEY,
	typename std::list<list_item>::iterator,
	Hash,
	KeyEqual> Map;

    Map map_;
    int capacity_;
    int size_;


public:
    LRUCache(const int capacity) : capacity_(capacity), size_(0)
    {
    }

    void add(const KEY &key, const VALUE &value) {
	auto iter = map_.find(key);
	if (iter != map_.end()) {
	    return;
	}

	// new entry
	if (would_overflow(map_.size(), capacity_)) {
	    evict();
	}

	items_.emplace_back(key, value);
	auto list_iter = items_.end();
	--list_iter;
	map_.insert(std::make_pair(key, list_iter));
	++size_;
    }

    static bool would_overflow(const int size, const int capacity) {
	return (size+1) > capacity;
    }

    void evict() {
	KEY key = items_.front().key;
	map_.erase(map_.find(key));
	items_.pop_front();
    }

    void remove(const KEY &key) {
	auto map_iter = map_.find(key);
	if (map_iter == map_.end()) {
	    return;
	}
	auto list_iter = map_iter->second;
	items_.erase(list_iter);
    }

    VALUE *get(const KEY &key) {
	auto map_iter = map_.find(key);
	if (map_.end() == map_iter) {
	    return 0;
	}

	// move the item to the back.
	auto list_iter = map_iter->second;
	list_item item_copy = *list_iter; // copy the element.

	items_.erase(list_iter);
	items_.push_back(std::move(item_copy));
	list_iter = items_.end();
	--list_iter;
	map_[key] = list_iter;

	return &items_.back().value;

    }

    void print(std::ostream &os) const {
	os << "list = ";
	for (auto &item : items_) {
	    os << " [ key = " << item.key << " value = " << item.value << " ] ";
	}
	os << "\n";
    }

};

} // close rlib

#endif
