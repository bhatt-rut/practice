#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <unordered_map>
#include <queue>
#include <cassert>

/* Given a list of comma separated vertex names and adjacent vertices
 * this program prints out graph searches
 *
     * Specify a to/from
 *
 * sample format:
 * s=r,w
 * r=s,v
 * w=s,t
 */

using namespace std;

struct vertex {
    string name;
    vector<string> adjacent;
    vertex *parent;
    int depth;

    vertex() : parent(0), depth(-1) {}
};

void print_vertex(const vertex &v) {
    cout << "name = " << v.name << " adjacents = [ ";
    copy(v.adjacent.begin(), v.adjacent.end(), ostream_iterator<string>(cout, " "));
    cout << "]\n";
}

vertex make_vertex(const string &line) {

    vertex v;
    int pos = line.find_first_of('=');
    v.name = line.substr(0, pos);

    string rest = line.substr(pos+1);

    // tokenize by commas.
    int spos = 0;
    while (spos < rest.size()) {
	int nextpos = rest.find_first_of(',', spos);
	if (string::npos == nextpos) { // no more tokens left.
	    v.adjacent.push_back(rest.substr(spos));
	    break;
	}

	v.adjacent.push_back(rest.substr(spos, nextpos-spos));
	spos = nextpos+1;
    }

    v.parent = 0;
    return v;
}

vertex *find_vertex(vector<vertex> &vertices, const string &to_find) {
    vertex *found = 0;
    for (auto & v : vertices) {
	if (to_find == v.name) {
	    found = &v;
	    break;
	}
    }
    assert(0 != found);
    return found;
}

void dfs(vector<vertex> &vertices, const string &start_name) {
    enum class Color { e_WHITE, e_GREY, e_BLACK };
    unordered_map<string, Color> node_color;

    auto & starting_vertex = *find_vertex(vertices, start_name);
    starting_vertex.parent = 0;
    starting_vertex.depth  = 0;
    queue<string *> visit_queue;

    visit_queue.push(&starting_vertex.name);

    while (!visit_queue.empty()) {

	string * current = visit_queue.front();
	visit_queue.pop();

	node_color[*current] = Color::e_GREY;
	vertex * parent = find_vertex(vertices, *current);

	for (string & name : parent->adjacent) {
	    auto found = node_color.find(name);

	    if (found != node_color.end()) {
		continue; // already visited.
	    }
	    node_color[name] = Color::e_GREY;
	    visit_queue.push(&name);

	    // also adjust the parent
	    vertex * v = find_vertex(vertices, name);
	    v->parent = parent;
	    v->depth  = parent->depth + 1;
	}

	node_color[*current] = Color::e_BLACK;
    }
}

void dfs_test(vector<vertex> &vertices, const string &start_name) {
    if (start_name.empty()) {
	dfs(vertices, vertices[0].name);
    } else {
	dfs(vertices, start_name);
    }

    for ( auto & v : vertices) {
	cout << "name = '" << v.name << "' at depth = " << v.depth
	    << " has parent = '" << (v.parent ? v.parent->name : "NULL") << "'\n";
    }
}

const string k_Start = "print-path=";

bool is_print_path_cmd(const string &line) {
    return line.substr(0, k_Start.size()) == k_Start;
}

pair<string, string> get_print_path_cmd(const string &line) {
    int comma_pos = line.find_first_of(',', k_Start.size());

    string from = line.substr(k_Start.size(), comma_pos-k_Start.size());
    string to   = line.substr(comma_pos+1);
    return make_pair(from, to);
}

bool print_path_impl(vertex *to, vertex *from) {
    if (to == 0) {
	cout << "no path\n";
	return false;
    }

    if (to == from) {
	cout << to->name;
	return true;
    } else {
	if (print_path_impl(to->parent, from)) {
	    cout << "->" << to->name;
	}
    }
    return true;
}

void print_path(vector<vertex> &vertices, const pair<string, string> &from_to) {
    cout << "finding path from '" << from_to.first << "' to '" << from_to.second << "'\n";

    vertex * to = find_vertex(vertices, from_to.second);
    vertex *from = find_vertex(vertices, from_to.first);

    print_path_impl(to, from);
    cout << "\n";
}


int main() {
    string line;

    vector<vertex> vertices;
    pair<string, string> *from_to = 0;
    while ( getline(cin, line) ) {

	if (is_print_path_cmd(line)) {
	    from_to = new pair<string, string>(get_print_path_cmd(line));
	    continue;
	}

	auto v = make_vertex(line);
	print_vertex(v);
	vertices.push_back(move(v));
    }

    dfs_test(vertices, from_to ? from_to->first : "");

    if (from_to != 0) {
	print_path(vertices, *from_to);
	delete from_to;
    }
}
