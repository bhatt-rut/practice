#include <lrucache.h>

int main() {
    using namespace rlib;

    typedef LRUCache<int,
		     std::string> cache_type;
    cache_type cache(5);

    cache.add(1, "one");
    cache.add(2, "two");
    cache.add(3, "three");
    cache.add(4, "four");
    cache.add(5, "five");

    cache.print(std::cout);

    std::string * three = cache.get(3);

    cache.print(std::cout);

    cache.add(6, "six");
    cache.print(std::cout);

}
