#ifndef INCLUDED_RLIB_CARTESIANPRODUCT
#define INCLUDED_RLIB_CARTESIANPRODUCT

#include <vector>
#include <algorithm>

namespace rlib {

struct cartesian_product {
    template<typename T>
    static std::vector<std::vector<T>> product(
	    const std::vector<std::vector<T>> &left,
	    const std::vector<std::vector<T>> &right);
};

template<typename T>
std::vector<std::vector<T>> cartesian_product::product(
	const std::vector<std::vector<T>> &left,
	const std::vector<std::vector<T>> &right)
{
    using namespace std;
    vector<vector<T>> result;
    result.reserve(left.size() * right.size());

    for (int i = 0; i < left.size(); ++i) {

	for (int j = 0; j < right.size(); ++j) {

	    vector<T> internal_product;
	    internal_product.reserve(left[i].size() + right[i].size());
	    copy(left[i].begin(), left[i].end(), back_inserter(internal_product));
	    copy(right[j].begin(), right[j].end(), back_inserter(internal_product));

	    result.emplace_back(internal_product);
	}
    }
    return result;
}


}

#endif
