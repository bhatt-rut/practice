#include <queue.h>
#include <cassert>
#include <iostream>

using queue = rlib::queue;

void small_test()
{
    queue q;
    q.push(10);
    int ret = q.pop();
    assert(ret == 10);
}

void big_test()
{
    queue q;
    for (int i = 0; i < 100; ++i) {
	q.push(i);
    }
    int popped = q.pop();
    assert(0 == popped);
    popped = q.pop();
    assert( 1== popped);
    for (int i = 0; i < 10; ++i) {
	q.pop();
    }
    popped = q.pop();
    assert(12 == popped);
}

int main()
{
    small_test();
    big_test();
    std::cout << "all done\n";
}
