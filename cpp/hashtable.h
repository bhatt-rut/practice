#ifndef INCLUDED_RLIB_HASHTABLE
#define INCLUDED_RLIB_HASHTABLE

#include <cassert>
#include <cstring>

namespace rlib {

template<typename T>
class Comparable {
public:
    bool operator()(const T &first, const T &second) const;
};

/* @brief Simple hash table implementation using a dynamic array and linked list */

// TODO list in order of priority.
// - ability to remove items.
// - growth functionality
// - shrink functionality
template<class KEY, class VALUE, template<typename> class HASHER>
class HashTable {
private:
    typedef HASHER<KEY> hash_func;
    typedef Comparable<KEY> compare;

public:
    HashTable(const HASHER<KEY> &hasher, const Comparable<KEY> &comparator);
    ~HashTable();

    bool add(const KEY &key, const VALUE &value);
    void remove(const KEY &value);
    bool get(const KEY &key, VALUE &value);

private:
    struct node {
	KEY key;
	VALUE value;
	node *next;
	node(const KEY &key, const VALUE &value) : key(key), value(value), next(0) {}
    };

    hash_func hasher;
    compare   comparator;
    int size;
    int capacity;
    node **array;

    void initialize(int sz);

    // add an already exiting node to an array. this facilitates growth ops.
    void add(node **array, const int index, node *node_to_insert);

    // grow the hash table.
    void grow();

    static void delete_list(node **node_ptr);

};

template<class KEY, class VALUE, template<typename> class HASHER>
HashTable<KEY, VALUE, HASHER>::HashTable(const HASHER<KEY> &hasher, 
	const Comparable<KEY> &comparator) :
    hasher(hasher), comparator(comparator), size(0), capacity(0), array(0)
{
    const int k_InitialSize = 10;
    capacity = k_InitialSize;
    array = new node*[capacity];

    // initialize to all zeros.
    std::memset(array, 0, capacity / sizeof(node *));
}

template<class KEY, class VALUE, template<typename> class HASHER>
void HashTable<KEY, VALUE, HASHER>::delete_list(node **nodes_ptr) {
    node * p = *nodes_ptr;
    if (0 == p) {
	return; // nothing to do.
    }

    while (p) {
	node *temp = p;
	p = p->next;
	delete temp;
    }

    *nodes_ptr = 0;
}

template<class KEY, class VALUE, template<typename> class HASHER>
HashTable<KEY, VALUE, HASHER>::~HashTable()
{
    for (int i = 0; i < capacity; ++i) {
	delete_list(&array[i]);
    }

    delete []array;
}



template<class KEY, class VALUE, template<typename> class HASHER>
bool HashTable<KEY, VALUE, HASHER>::add(const KEY &key, const VALUE &value)
{
    const int hash_val = hasher(key);
    const int index    = hash_val % capacity;

    // naive growth strategy.
    if (1 + size >= capacity) {
	this->grow();
    }

    assert(index < this->capacity);

    // find the correct node ptr.
    node ** index_pos = &array[index];

    if (0 == *index_pos) { // first entry in this position in the hash table
	*index_pos = new node(key, value);
    } else {
	node *insert_pos = *index_pos;
	while (insert_pos != 0) {
	    if (insert_pos->key == key) { // key is already in table.
		return false;
	    }
	    if (insert_pos->next == 0) {
		break;
	    }
	    insert_pos = insert_pos->next;
	}
	insert_pos->next = new node(key, value);
    }
    ++size;
    return true;
}

template<class KEY, class VALUE, template<typename> class HASHER>
void HashTable<KEY, VALUE, HASHER>::add(node **array, const int index, 
					node *node_to_insert) {

    // find the current list at the position in the array
    node ** index_pos = &array[index];

    if (0 == *index_pos) { // this is a new entry
	*index_pos = node_to_insert;
    } else {
	node *insert_pos = *index_pos;
	while (insert_pos->next != 0) {
	    insert_pos = insert_pos->next;
	}

	insert_pos->next = node_to_insert;
    }
}

template<class KEY, class VALUE, template<typename> class HASHER>
bool HashTable<KEY, VALUE, HASHER>::get(const KEY &key, VALUE &value)
{
    const int hash_val = hasher(key);
    const int index    = hash_val % capacity;

    assert(index < this->capacity);

    node * fetch_pos = array[index];
    if (0 == fetch_pos) {
	return false;
    }

    while (fetch_pos != 0) {
	if (fetch_pos->key == key) {
	    value = fetch_pos->value;
	    return true;
	}
	fetch_pos = fetch_pos->next;
    }
    return false;
}

template<class KEY, class VALUE, template<typename> class HASHER>
void HashTable<KEY, VALUE, HASHER>::grow() {
    const int k_GrowthFactor = 2;

    // initialize new bigger array
    const int new_size = capacity * k_GrowthFactor;

    node ** temp_array = new node*[new_size];

    // rehash and copy
    for (int i = 0; i < capacity; ++i) {
	node * list_node = array[i];

	while (list_node != 0) {
	    int hash_val = hasher(list_node->key);
	    int index    = hash_val % new_size;

	    node * current = list_node;

	    node * next    = list_node->next;
	    current->next  = 0; // reset the of this item that's going to another list
	    add(temp_array, index, current);
	    list_node = next;
	 }
    }

    // finished moving everything to the new array.
    delete []array;
    array = temp_array;
    capacity = new_size;
}

} // close namespace rlib

#endif
