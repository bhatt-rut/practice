#include <cartesian-product.h>

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

int main()
{
    using namespace std;
    using namespace rlib;
    string s = "gl";
    
    vector<char> g;
    vector<char> l;
    g.push_back('g');
    l.push_back('l');

    vector<vector<char>> left;
    vector<vector<char>> right;
    
    left.push_back(g);
    left.push_back(l);

    right.push_back(g);
    right.push_back(l);

    auto result = cartesian_product::product(left, right);

    vector<string> twochars;
    twochars.reserve(result.size());
    for (auto & vec : result) {
	string res;
	copy(vec.begin(), vec.end(), back_inserter(res));
	cout << res << "\n";
	twochars.push_back(res);
    }

    vector<vector<char>> twochars_c;
    for (auto & s : twochars) {
	vector<char> transformed;
	copy(s.begin(), s.end(), back_inserter(transformed));
	twochars_c.emplace_back(transformed);
    }

    cout << "level 2:\n";
    auto result_2 = cartesian_product::product(twochars_c, left);
    for (auto & vec : result_2) {
	string res;
	copy(vec.begin(), vec.end(), back_inserter(res));
	cout << res << "\n";
    }

    return 0;
}
