#ifndef INCLUDED_SORT
#define INCLUDED_SORT

#include <vector>

namespace rlib {

struct sort {

// iterator must satisfy random access iterator.


// in place version of the merge sort.
static void merge_sort(std::vector<int> &numbers);

// in place version of quick sort.
static void quick_sort(std::vector<int> &numbers);

};

} // close namespace rlib

#endif
