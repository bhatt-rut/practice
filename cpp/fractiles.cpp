// this is for https://code.google.com/codejam/contest/6254486/dashboard#s=p3

#include <iostream>
#include <vector>
#include <sstream>
#include <cassert>
#include <map>
#include <set>
#include <algorithm>

using namespace std;

struct Coordinate {
    int row;
    int col;

    Coordinate(int row, int col) : row(row), col(col) {}
};

typedef vector<Coordinate> Coordinates;
typedef vector<Coordinates> L_CountVec;

int num_possibilities(int k)
{
    if (0 == k) {
	return 0;
    }
    int n = 2;
    for(; 0 < k - 1; --k) {
	n *= 2;
    }
    return n;
}

struct node {
    char c;
    node *left;
    node *right;
    node *parent; // to know how to walk back up from bottom.

    node(char c) : c(c), left(0), right(0), parent(0)
    {}
};

string build_str(node *node) {
    string str;
    while(node) {
	str.append(1, node->c);
	node = node->parent;
    }
    return str;
}

void make_tree(int depth, vector<node *> &nodes, node *parent, vector<string> &strs)
{
    if (depth == 0) {
	strs.push_back(move(build_str(parent)));
	return;
    }

    node *left = new node{'G'};
    node *right = new node{'L'};

    parent->left = left;
    parent->right = right;
    left->parent = parent;
    right->parent = parent;
    nodes.push_back(left);
    nodes.push_back(right);


    make_tree(depth-1, nodes, left, strs);
    make_tree(depth-1, nodes, right, strs);
}

vector<string> originals(int k)
{
    vector<string> all;
    all.reserve(num_possibilities(k));

    node *g = new node{'G'};
    node *l = new node{'L'};
    vector<node *> nodes = {g, l};
    make_tree(k-1, nodes, g, all);
    make_tree(k-1, nodes, l, all);

    for (node *node : nodes) {
	delete node; // no longer needed.
    }

    cout << "finished building strings\n";
    return all;
}

string solve_complexity(const string &orig, const string &last, int k, int complexity)
{
    if (0 == complexity)
	return last;

    ostringstream os;
    for (char c : last) {
	if (c == 'L') {
	    os << orig;
	}
	else {
	    for (int i = 0; i < k; ++i) {
		os << 'G';
	    }
	}
    }

    return solve_complexity(orig, os.str(), k, --complexity);
}

string solve_puzzle(const vector<string> &fractiles, map<int, set<int>> &groups, 
		    int num_solvers)
{
    assert(0 < fractiles.size());
    // special case of when there are more solvers than number of tiles
    if (num_solvers >= fractiles[0].size()) {
	string out;
	for (int i = 1; i <= fractiles[0].size(); ++i) {
	    out.append(to_string(i)).append(" ");
	}
	return out;
    }

    //for (auto & group : groups) {
	//cout << group.first << ":";
	//for (auto col : group.second) {
	    //cout << " " << (1+col);
	//}
	//cout << "\n";
    //}

    auto is_last = [&fractiles](int index) {
	return index == (fractiles.size()-1);
    };
    string out;
    for (auto &group : groups) {
	if (is_last(group.first))
	    continue;

	out.append(to_string(1+(*group.second.begin()))).append(" ");

	if (num_solvers == 0) { // exhausted all solvers
	    break;
	}
    }

    return out;
}

L_CountVec get_lcounts(const vector<string> &fractiles, int num_helpers)
{
    L_CountVec all_lcounts;

    auto len = fractiles[0].size();
    for (int i = 0; i < len; ++i) { // col
	Coordinates coords;
	bool too_many_ls = false;
	for (int j = 0; j < fractiles.size(); ++j) {
	    if (fractiles[j][i] != 'L')
		continue;

	    // found an L, store its coordinates.
	    coords.push_back(move(Coordinate{j, i}));
	    if (coords.size() > num_helpers) { // is this a valid thing?
		too_many_ls = true;
		break;
	    }
	}

	if (!too_many_ls) {
	    all_lcounts.push_back(move(coords));
	}
    }
    return all_lcounts;
}

map<int, set<int>> group_by_row(const L_CountVec &lcounts, set<int> &full_col_set, 
				int num_rows)
{
    map<int, set<int>> groups;
    for (const Coordinates & coords : lcounts) {
	for (auto coord : coords) {
	    groups[coord.row].insert(coord.col);
	    if (coord.row != num_rows-1) {
		full_col_set.insert(coord.col);
	    }
	}
    }

    return groups;
}

void solve(int case_num, const string &line)
{
    cout << "Case #" << case_num << ": ";
    istringstream is(line);
    int k = 0, c = 0, s = 0;
    is >> k >> c >> s;

    //cout << "k = " << k << " c = " << c << " s = " << s << "\n\n";
    vector<string> origs{move(originals(k))};
    vector<string> fractiles;
    fractiles.reserve(origs.size());

    for (auto &str : origs) {
	fractiles.push_back(move(solve_complexity(str, str, k, c-1)));
    }

    //for (auto & frac : fractiles) {
	//cout << frac << "\n";
    //}

    auto l_counts{move(get_lcounts(fractiles, s))};

    set<int> full_col_set;
    auto groups{move(group_by_row(l_counts, full_col_set, fractiles.size()))};
    if (groups.empty()) {
	cout << "IMPOSSIBLE\n";
	return;
    }

    // grab the l columns for the last group...
    auto & last_row = groups[fractiles.size()-1];

    vector<int> diff;
    set_difference(last_row.begin(), last_row.end(), 
	             full_col_set.begin(), full_col_set.end(),
		     back_inserter(diff));
    if (diff.size() > 0) {
	cout << (1+diff[0]) << "\n";
	return;
    }

    cout << solve_puzzle(fractiles, groups, s) << "\n";
}

int main() {
    string input_line;
    vector<string> lines;
    
    while (getline(cin, input_line)) {
	lines.push_back(input_line);
    }

    const int count = stoi(lines[0]);
    assert(count == (lines.size()-1));
    lines.erase(lines.begin()); // confirmed it's ok, chuck it.

    int case_num = 1;
    for (auto & str : lines) {
	solve(case_num++, str);
    }
    return 0;


}
