#include <vector>
#include <iostream>

#include <graph-printer.h>

using namespace std;

struct tree_node {
    int val;
    tree_node *left;
    tree_node *right;

    tree_node() : val(0), left(0), right(0) {}
};

tree_node *make_tree_from_array(int start_index, int end_index, vector<int> &nums, vector<tree_node *> &nodes)
{
    if (start_index == end_index) {
	return 0;
    }
    int midpoint = (end_index - start_index) / 2 + start_index;
    tree_node * node = new tree_node{};
    nodes.push_back(node);
    node->val = nums[midpoint];

    if (start_index == end_index || midpoint == start_index ||
	    midpoint == end_index)
	return node;

    node->left = make_tree_from_array(start_index, midpoint, nums, nodes);
    node->right = make_tree_from_array(midpoint+1, end_index, nums, nodes);

    cout << "node = " << node->val << " has children = [ ";
    if (node->left) {
	cout << "left = " << node->left->val << " ";
    }
    if (node->right) {
	cout << "right = " << node->right->val << " ";
    }
    cout << "]\n";

    return node;
}

void other_test()
{
    vector<int> nums{1, 3, 4, 6, 8, 10, 12, 14};
    vector<tree_node *> nodes;

    tree_node * root = make_tree_from_array(0, nums.size(), nums, nodes);

    for (tree_node *node : nodes) {
	delete node;
    }
}

int main()
{
    vector<int> nums{1, 3, 4, 6, 8, 10, 12};

    vector<tree_node *> nodes;

    tree_node * root = make_tree_from_array(0, nums.size(), nums, nodes);

    for (tree_node *node : nodes) {
	delete node;
    }

    cout << "other test:\n";
    other_test();


    cout << "done\n";
    
}
