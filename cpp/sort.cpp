#include <sort.h>

#include <algorithm>
#include <iostream>
#include <iterator>

namespace rlib {
using namespace std;

namespace {

// the left and the right should be sorted with respect to each other as the merge
// is applied from the bottom up.
void merge(vector<int> &numbers, int min, int mid, int max, vector<int> &work) {
    // copy things into temporary space.
    for (int i = min ; i <= max ; ++i) {
	work[i] = numbers[i];
    }

    int left_helper = min;
    int right_helper = mid + 1;
    int current = min; // indicates the insertion position

    while (left_helper <= mid && right_helper <= max) {
	if (work[left_helper] <= work[right_helper]) {
	    // copy back into original place.
	    numbers[current] = work[left_helper];
	    ++left_helper;
	} else {
	    numbers[current] = work[right_helper];
	    ++right_helper;
	}
	++current;
    }

    const int num_to_copy = mid - left_helper;
    for (int i = 0; i <= num_to_copy; ++i) {
	numbers[i+current] = work[left_helper+i];
    }
}

void merge_sort_impl(int min, int max, vector<int> &numbers, vector<int> &work) {

    int diff = max - min;
    if (0 == diff) { // one item, already sorted.
	return;
    }

    const int mid = (max+min)/2;

    merge_sort_impl(min, mid, numbers, work);
    merge_sort_impl(mid+1, max, numbers, work);

    merge(numbers, min, mid, max, work);
}

void swap(vector<int> &numbers, int left, int right) {
    int temp = numbers[left];
    numbers[left] = numbers[right];
    numbers[right] = temp;
}

int partition(vector<int> &numbers, int min, int max) {
    // choose pivot in the middle
    const int pivot = numbers[(min+max)/2];
    while (min <= max) {
	// walk until there is an element that doesn't belong to the left
	// of the pivot
	while ( pivot > numbers[min] ) { ++min; }
	
	// walk left until there is an element that doesn't belong to
	// the right of the pivot
	while ( pivot < numbers[max] ) { --max; }

	if ( min <= max) { // == to still change the index.
	    swap(numbers, min, max);
	    ++min;
	    --max;
	}
    }
    return min;
}

void quick_sort_impl(vector<int> &numbers, int min, int max) {
    const int partition_index = partition(numbers, min, max);

    if (partition_index - 1 > min) {
	quick_sort_impl(numbers, min, partition_index-1);
    }
    if (partition_index < max) {
	quick_sort_impl(numbers, partition_index, max);
    }

}

} // close anonymous namespace


void sort::merge_sort(vector<int> &numbers) {
    // allocate a new vector for doing work.

    vector<int> temp;
    temp.resize(numbers.size());
    fill(temp.begin(), temp.end(), 0);

    merge_sort_impl(0, numbers.size()-1, numbers, temp);
}

void sort::quick_sort(vector<int> &numbers) {
    quick_sort_impl(numbers, 0, numbers.size()-1);    
}

} // close namespace rlib
