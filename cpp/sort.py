
def swap(arr, i, j):
    temp = arr[i]
    arr[i] = arr[j]
    arr[j] = temp

def qsort(arr, l, u, threshold=None):
    if threshold is None:
        threshold = 0
    else:
        print "threshold = ", threshold

    if (u-l) <= threshold:
        return

    m = l + 1
    d = u
    pivot = arr[l]

    while m <= d:
        while m <= u and arr[m] < pivot:
            m += 1
        while arr[d] > pivot:
            d -= 1

        if m > d:
            break
        swap(arr, m, d)
        m += 1
        d -= 1

    swap(arr, l, d)
    qsort(arr, l, d-1)
    qsort(arr, d+1, u)

def quick_sort(arr, l, u, threshold=None):
    if threshold is None:
        threshold = 0
    if (u-l) <= threshold:
        return

    m = l
    pivot = arr[l]
    d = u
    while m <= d:

        while arr[m] < pivot:
            m += 1
        while arr[d] > pivot:
            d -= 1

        if m > d:
            break

        swap(arr, m, d)
        m += 1
        d -= 1

    quick_sort(arr, l, m-1)
    quick_sort(arr, m, u)

def merge(arr, work, l, m, h):
    # copy the original stuff into work
    for i in range(l, h+1):
        work[i] = arr[i]

    lh = l
    rh = m+1
    current = l

    while lh <= m and rh <= h:
        if work[lh] <= work[rh]:
            arr[current] = work[lh]
            lh += 1
        if work[rh] < work[lh]:
            arr[current] = work[rh]
            rh += 1
        current += 1

    # is there anything left in the left partition?
    diff = m - lh
    for i in range(diff+1):
        arr[i+current] = work[i+lh]


def mergesort(arr, work, l, u):
    if l >= u:
        return

    mid = (l+u)/2
    mergesort(arr, work, l, mid)
    mergesort(arr, work, mid+1, u)
    merge(arr, work, l, mid, u)

def selectionsort(arr):
    for i in range(len(arr)):
        j = i
        # continuously swap elements down until everything is sorted
        while j > 0 and arr[j-1] > arr[j]:
            swap(arr, j-1, j)
            j -= 1

def combination_sort(arr):
    qsort(arr, 0, len(arr)-1, 2)
    print "after qsort: ", arr
    selectionsort(arr)

def main():
    arr = [3,1,2,5,6,10,32,100,4,11]
    arr2 = arr
    arr3 = arr
    arr4 = arr
    print "quick sort:"
    qsort(arr, 0, len(arr)-1)
    print arr

    print "merge sort:"
    work = arr2
    mergesort(arr2, work, 0, len(arr2)-1)
    print arr2

    print "combination sort (qsort + selection sort):"
    combination_sort(arr3)
    print arr3

    print "selection sort:"
    selectionsort(arr4)
    print arr4


if "__main__" == __name__:
    main()


