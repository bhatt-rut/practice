#ifndef INCLUDED_RLIB_QUEUE
#define INCLUDED_RLIB_QUEUE

namespace rlib {

class queue {
    private:
	struct queue_node {
	    int data;
	    queue_node *next;
	    queue_node(int x): data(x), next(0) {}
	};

	int d_size;
	queue_node *d_first;

    public:
	queue();
	~queue();
	void push(int x);
	int pop();

};

} // close namespace rlib

#endif
