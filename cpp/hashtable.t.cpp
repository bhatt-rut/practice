#include <hashtable.h>
#include <functional>
#include <iostream>
#include <string>

using namespace rlib;

class IntComparable : public Comparable<int> {
    bool operator()(const int &first, const int &second) const {
	return first == second;
    }
};

template<typename T>
struct terrible_hash {};

template<>
struct terrible_hash<int> {
    bool operator()(const int val) {
	return val %10;
    }
};

void growth_test() {
    IntComparable comp;
    HashTable<int, std::string, terrible_hash> tbl(terrible_hash<int>(), comp);

    for (int i = 0; i < 10; ++i) {
	std::string s = "number-";
	s.append(std::to_string(i));
	const bool success = tbl.add(i, s);
	std::cout << "success = " << std::boolalpha << success << "\n";
    }

    // add 1 more to push it over capacity
    bool success = tbl.add(10, "over-capacity");
    assert(success);

    std::string out;
    success = tbl.get(10, out);
    assert(out == "over-capacity");

    success = tbl.add(12, "twelve");
    assert(success);
    success = tbl.get(12, out);
    assert(out == "twelve");
}

void try_with_terrible_hash()
{
    IntComparable comp;
    HashTable<int, std::string, terrible_hash> tbl(terrible_hash<int>(), comp);

    bool success = tbl.add(10, "test");
    std::cout << std::boolalpha << success << "\n";
    success = tbl.add(20, "robot");
    std::cout << std::boolalpha << success << "\n";
    std::string got;
    success = tbl.get(10, got);
    assert(success);
    std::cout << got << "\n";
    got.clear();
    success = tbl.get(20, got);
    assert(success);
    std::cout << got << "\n";

}

int main() {
    IntComparable comp;
    HashTable<int, int, std::hash> tbl(std::hash<int>(), comp);

    bool success = tbl.add(10, 20);
    std::cout << std::boolalpha << success << "\n";

    success = tbl.add(9, 18);
    std::cout << std::boolalpha << success << "\n";

    success = tbl.add(9, 21);
    std::cout << std::boolalpha << success << "\n";
    assert(!success);

    int got = 0;
    success = tbl.get(9, got);
    assert(success);
    std::cout << got << "\n";

    try_with_terrible_hash();
    growth_test();
}
