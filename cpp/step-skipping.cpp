#include <iostream>

#include <vector>

int skip_steps(int steps_left)
{
    // only one way to go with no steps left.
    if (0 == steps_left)
	return 1;

    // only one way to go with 1 step left.
    if (2 > steps_left) {
	return skip_steps(steps_left - 1);
    }
    else if (3 > steps_left) {
	// with 2 steps, there are two ways (1 -> 1), (2)
	return skip_steps(steps_left - 2) + skip_steps(steps_left - 1);
    }

    return skip_steps(steps_left - 3) 
	 + skip_steps(steps_left - 2) 
	 + skip_steps(steps_left - 1);
}

// steps[0] = 1
// steps[1] = 1
// steps[2] = steps[0] + steps[1]
// steps[3] = steps[0] + steps[1] + steps[2]
// steps[4] = steps[1] + steps[2] + steps[3]
// steps[5] = steps[2] + steps[3] + steps[4]
int step_skip_iterative(int steps_left)
{
    using namespace std;
    vector<int> possibilities;
    possibilities.push_back(1);
    possibilities.push_back(1);
    possibilities.push_back(2);

    for (int i = 3; i <= steps_left; ++i) {
	possibilities.push_back(
		possibilities[i-3] +
		possibilities[i-2] +
		possibilities[i-1]);
    }

    return possibilities[steps_left];
}

int main()
{
    std::cout << "for 3 = " << skip_steps(3) << "\n";
    std::cout << "for 4 = " << skip_steps(4) << "\n";
    std::cout << "for 5 = " << skip_steps(5) << "\n";

    std::cout << "for 3 = " << step_skip_iterative(3) << "\n";
    std::cout << "for 4 = " << step_skip_iterative(4) << "\n";
    std::cout << "for 5 = " << step_skip_iterative(5) << "\n";
}
