#include <sort.h>

#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>

int main() {
    using namespace std;
    vector<int> nums{ 3, 8, 4, 2, 1, 5, 6, 0, 12, 14 };
    vector<int> more_nums = nums;

    rlib::sort::merge_sort(nums);

    cout << "(mergesort) result = ";
    copy(nums.begin(), nums.end(), ostream_iterator<int>(cout, " "));
    cout << "\n";

    rlib::sort::quick_sort(more_nums);
    cout << "(quicksort) result = ";
    copy(more_nums.begin(), more_nums.end(), ostream_iterator<int>(cout, " "));
    cout << "\n";
}
