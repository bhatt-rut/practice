#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

struct node {
    bool visited;
    bool visitable;
    int xpos;
    int ypos;

    // only consider two directions because we never need to or should move any other way
    node *right_child;
    node *down_child;

    node(bool visitable, int xpos, int ypos) : 
	visited(false), visitable(visitable), 
	xpos(xpos), ypos(ypos), right_child(0), down_child(0)
    {
    }
};

void build_tree(node *from_node, int grid [][3], int xdim, int ydim, vector<node *> &nodes)
{
    int xpos = from_node->xpos;
    int ypos = from_node->ypos;
    node *right = 0, *down = 0;
    if (xpos+1 < xdim) {
	right = new node((bool)grid[xpos+1][ypos], xpos+1, ypos);
	nodes.push_back(right);
	build_tree(right, grid, xdim, ydim, nodes);
    }

    if (ypos+1 <ydim) {
	down = new node((bool)grid[xpos][ypos+1], xpos, ypos+1);
	nodes.push_back(down);
	build_tree(down, grid, xdim, ydim, nodes);
    }
    from_node->right_child = right;
    from_node->down_child  = down;
}

bool walk(node *from, vector<node *> &path)
{
    if (0 == from)
	return false;
    // if this node is not visitable, terminate parent's path.
    if (!from->visitable) {
	return false;
    }

    // if this is the last node, we are finished.
    if (from->xpos == 2 && from->ypos == 2) {
	return true;
    }

    // otherwise, walk right then walk down...

    bool success = walk(from->right_child, path);
    if (success) {
	path.push_back(from->right_child);
    } else {
	success = walk(from->down_child, path);
	if (success) {
	    path.push_back(from->down_child);
	}
    }
    return success;
}

void solve(int grid[][3], size_t xdim, size_t ydim)
{
    vector<node *> nodes; // for deletion.
    node *root = new node(true, 0, 0); // root is always visitable
    nodes.push_back(root);

    build_tree(root, grid, xdim, ydim, nodes);

    vector<node *> path;
    bool found = walk(root, path);
    if (!found) {
	cout << "no path found\n";
	return;
    }

    reverse(path.begin(), path.end());
    for (auto *node_p : path) {
	cout << "x = " << node_p->ypos << " y = " << node_p->xpos << "\n";
    }

    for (auto *node_p : nodes) {
	delete node_p;
    }
}

int main()
{
    int grid[3][3] =
    {
	{ 1, 1, 0 },
	{ 0, 1, 1 },
	{ 1, 1, 1 }
    };
    size_t xdim = 3;
    size_t ydim = 3;

    solve(grid, xdim, ydim);
}
