#include <iostream>
#include <cassert>

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
	int carry = 0;

	ListNode *head = 0;
	ListNode *current = 0;

	while (l1 != NULL || l2 != NULL || carry != 0) {

	    int l1_num = l1 ? l1->val : 0;
	    int l2_num = l2 ? l2->val : 0;
	    int sum = l1_num + l2_num + carry;

	    if (0 == head) {
		head = new ListNode(sum%10);
		    // no carry at this point
		current = head;
	    } else {
		ListNode * node = new ListNode(sum%10);
		current->next = node;
		current       = node;
	    }
	    carry = sum > 9 ? 1 : 0;

	    if (l1 != NULL) { l1 = l1->next; }
	    if (l2 != NULL) { l2 = l2->next; }
	}

	return head;
    }
};

void test_nulls() {
    Solution sln;
    ListNode * node = sln.addTwoNumbers(0, 0);
    assert(0 == node);
}

void test_case1() {
    // uneven
    ListNode l1_n1{2};
    ListNode l1_n2{7};

    ListNode l2_n1{9};

    l1_n1.next = &l1_n2;

    Solution sln;
    ListNode * node = sln.addTwoNumbers(&l1_n1, &l2_n1);
    ListNode * current = node;
    while (current != NULL) {
	std::cout << current->val << " ";
	current = current->next;
    }
    std::cout << "\n";
}

void test_case2() {
    ListNode l1_n1{5};
    ListNode l2_n1{5};
    Solution sln;
    ListNode * node = sln.addTwoNumbers(&l1_n1, &l2_n1);
    ListNode * current = node;
    while (current != NULL) {
	std::cout << current->val << " ";
	current = current->next;
    }
    std::cout << "\n";
}

int main() {

    ListNode l1_n1{2};
    ListNode l1_n2{4};
    ListNode l1_n3{3};

    ListNode l2_n1{5};
    ListNode l2_n2{6};
    ListNode l2_n3{4};

    l1_n2.next = &l1_n3;
    l1_n1.next = &l1_n2;

    l2_n2.next = &l2_n3;
    l2_n1.next = &l2_n2;

    Solution sln;
    ListNode * node = sln.addTwoNumbers(&l1_n1, &l2_n1);

    ListNode * current = node;
    while (current != NULL) {
	std::cout << current->val << " ";
	current = current->next;
    }
    std::cout << "\n";

    test_nulls();
    test_case1();
    test_case2();

}
