#include <iostream>
#include <vector>
#include <stdexcept>
#include <stack>

using namespace std;

class NestedInteger {
public:
 // Return true if this NestedInteger holds a single integer, rather than a nested list.
 bool isInteger() const;

 // Return the single integer that this NestedInteger holds, if it holds a single integer
 // The result is undefined if this NestedInteger holds a nested list
 int getInteger() const;

 // Return the nested list that this NestedInteger holds, if it holds a nested list
 // The result is undefined if this NestedInteger holds a single integer
 const vector<NestedInteger> &getList() const;
};

class NestedIterator {
public:
        NestedIterator(vector<NestedInteger> &nestedList) {
	state_.push(node_pos(&nestedList, 0));
    }

    int next() {
	// let's try a different approach.

	node_pos * node = &state_.top();

	// keep digging until a plain integer is found.
	while (!node.ptr[node->pos].isInteger()) {
	    node_pos * current = node;
	    auto *topnow = (current->ptr)[current->pos].getList();
	    ++current->pos; // advance iterator for having explored this one already.

	    state_.push(node_pos{topnow, 0}, 0);
	}
	
	// at an integer.
	int ret = node->ptr[node->pos].getInteger();
	node->pos++;

	while (!state_.empty() &&
		node->pos >= node->ptr->size()) {
	    state_.pop();
	    if (!state_.empty()) {
		node = &state_.top();
	    }
	}

	return ret;
    }

    bool hasNext() {
	return !state_.empty();
    }

private:
    struct node_pos {
	const vector<NestedInteger> * ptr;
	int                     pos;
	node_pos(const vector<NestedInteger> *ptr, int pos) : ptr(ptr), pos(pos) {}
    };
    stack<node_pos> state_;
};

int main() {

    //NestedIterator i(nestedList);
    //while (i.hasNext()) { cout << i.next() << "\n"; }
}
