#include <iostream>
#include <queue>
#include <unordered_map>

using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
private:
    struct Scores { int with_me; int without_me; Scores() : with_me(0), without_me(0) {} };
    unordered_map<TreeNode *, Scores> scores;

public:
    static int max(const Scores &scores) {
	if (scores.with_me > scores.without_me) {
	    return scores.with_me;
	}
	return scores.without_me;
    }

    int rob(TreeNode *root) {
	if (NULL == root) { return 0; }
	// post order traversal.

	rob(root->left);
	rob(root->right);

	Scores &my_scores = scores[root];
	my_scores.with_me = root->val;
	my_scores.with_me += (root->left == 0) ? 0 : scores[root->left].without_me;
	my_scores.with_me += (root->right == 0) ? 0 : scores[root->right].without_me;

	my_scores.without_me += (root->left == 0) ? 0 : max(scores[root->left]);
	my_scores.without_me += (root->right == 0) ? 0 : max(scores[root->right]);

	return max(my_scores);
    }

};

void test_case2() {
    Solution sln;

    TreeNode l2n1{1};
    TreeNode l2n2{3};
    TreeNode l2n3{1};
    TreeNode l1n1{4}; l1n1.left = &l2n1; l1n1.right = &l2n2;
    TreeNode l1n2{5}; l1n2.right = &l2n3;
    TreeNode root{3}; root.left = &l1n1; root.right = &l1n2;

    cout << sln.rob(&root) << "\n";
}

int main() {
    Solution sln;

    TreeNode l2n1{3};
    TreeNode l2n2{1};
    TreeNode l1n1{2}; l1n1.right = &l2n1;
    TreeNode l1n2{3}; l1n2.right = &l2n2;
    TreeNode root{3}; root.left = &l1n1; root.right = &l1n2;

    cout << sln.rob(&root) << "\n";

    test_case2();
}
