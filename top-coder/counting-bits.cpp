#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

// given a number, return the number of 1s in binary
class Solution {
public:
    vector<int> num_ones(const int num) {
	vector<int> nums;
	for (int j = 0; j <= num; ++j) {
	    int count = 0;
	    // naive approach
	    for (int i = 0; i < 32; ++i) {
		if ((j >> i) & 0x0001) {
		    count += 1;
		}
	    }
	    nums.push_back(count);
	}
	return nums;
    }

};

int main() {
    Solution sln;
    auto out = sln.num_ones(5);
    copy(out.begin(), out.end(), ostream_iterator<int>(cout, " "));
    cout << "\n";
}
