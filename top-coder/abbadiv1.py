import sys 

def reverse(s):
    l = list(s)
    l.reverse()
    return ''.join(l)

def solve(initial, target):
    if len(initial) == len(target):
        return initial == target

    temp = initial +'A'
    if target.find(temp) != -1 or target.find(reverse(temp)) != -1:
        return solve(temp, target)

    temp = initial + 'B'
    if target.find(temp) != -1 or target.find(reverse(temp)) != -1:
        return solve(reverse(temp), target)


class ABBADiv1:
    def canObtain(self, initial, target):
        repeat = lambda letter, num_times: num_times * letter
        if target[0] != 'B':
            # the only allowed move is adding As.
            result = initial + repeat('A', len(target)-len(initial))
            if result == target:
                return "Possible"
            else:
                return "Impossible"
        else:
            if solve(initial, target):
                return "Possible"
            else:
                return "Impossible"
            
d = ABBADiv1()
print d.canObtain("A", "BABA")
print d.canObtain("BAAAAABAA", "BAABAAAAAB")
print d.canObtain("A", "ABBA")
print d.canObtain("AAABBAABB", "BAABAAABAABAABBBAAAAAABBAABBBBBBBABB")
print d.canObtain("AAABAAABB", "BAABAAABAABAABBBAAAAAABBAABBBBBBBABB")
