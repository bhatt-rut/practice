#include <iostream>

class Solution {
public:
    int integerBreak(int n) {
	switch (n) {
	case 2:
	    return 1;
	case 3:
	    return 2;
	}
	return calc(n);
    }
private:
    static int calc(int n) {
	int rem = n % 3;
	int result = (rem == 0) ? 1 : (rem == 1 ? 4 : 2);
	int max    = (rem == 1) ? (n/3 - 1) : (n/3);
	for (int i = 0 ; i < max ; ++i) { result *= 3; }

	return result;
    }
};

int main() {
    Solution s;
    std::cout << s.integerBreak(2) << "\n";
    std::cout << s.integerBreak(3) << "\n";
    std::cout << s.integerBreak(4) << "\n";
    std::cout << s.integerBreak(5) << "\n";
    std::cout << s.integerBreak(6) << "\n";
    std::cout << s.integerBreak(7) << "\n";
    std::cout << s.integerBreak(8) << "\n";
    std::cout << s.integerBreak(9) << "\n";
    std::cout << s.integerBreak(10) << "\n";
    std::cout << s.integerBreak(11) << "\n";
    std::cout << s.integerBreak(12) << "\n";
    std::cout << s.integerBreak(13) << "\n";
}
