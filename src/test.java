import org.jetbrains.annotations.NotNull;

class test {
    @NotNull
    public static String joinStrings(String []strs) {
        StringBuilder builder = new StringBuilder();

        for (String str : strs) {
            builder.append(str);
        }

        return builder.toString();
    }

    public static String joinStringsMine(String [] strs) {
        MyStringBuilder builder = new MyStringBuilder();
        for (String str : strs) {
            builder.append(str);
        }
        return builder.toString();
    }

    public static void LinkedListTest() {
        int [] integers = {
                1, 2, 3, 4, 5
        };

        LinkedList list = new LinkedList();
        for (int x : integers) {
            list.append(x);
        }

        list.print();

        list.remove(3);
        list.print();
        System.out.println(list.length());

        list.remove(4);
        list.print();
        System.out.println(list.length());

        list.remove(1);
        list.remove(2);
        list.remove(5);
        list.print();
        System.out.println(list.length());

        list.remove(10);
        list.print();
        System.out.println(list.length());

        // partitioning test
        {
            int[] moreInts = {
                    3, 5, 8, 5, 10, 2, 1
            };
            list = new LinkedList();
            for (int x : moreInts) {
                list.append(x);
            }
            list.print();
            list.partition(5);
            list.print();
        }

        {
            int[] moreInts = {
                    10, 5, 8, 5, 10, 2, 1
            };
            list = new LinkedList();
            for (int x : moreInts) {
                list.append(x);
            }
            list.print();
            list.partition(8);
            list.print();

        }


    }

    public static void main(String [] args) {
        String [] strings = {
                "this",
                "that"
        };
        System.out.println(joinStrings(strings));

        System.out.println(joinStringsMine(strings));

        LinkedListTest();
    }
}