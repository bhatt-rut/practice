/**
 * Created by rutvij on 4/3/16.
 */
public class LinkedList {
    private Node d_head = null;

    public void append(int x) {
        if (null == d_head) {
            d_head = new Node(x);
            return;
        }

        Node walker = d_head;
        // walk to the last element
        while (walker.getNext() != null) {
            walker = walker.getNext();
        }

        Node new_node = new Node(x);
        walker.setNext(new_node);
    }

    public Node next(Node given) {
        return given.getNext();
    }

    public int length() {
        Node walker = d_head;
        int len = 0;
        for (; walker != null; walker = walker.getNext()) {
            ++len;
        }
        return len;
    }

    public Node remove(int y) {
        Node removed = null;
        Node walker = d_head;

        if (null == d_head) { // do nothing for empty.
            return removed;
        }

        // special case of removing the head of the list
        if (d_head != null && d_head.getValue() == y) {
            removed = d_head;
            d_head = d_head.getNext();
            return removed;
        }

        while (walker.getNext() != null) {
            if (y == walker.getNext().getValue()) {
                removed = walker.getNext();
                // found the node to remove
                walker.setNext(walker.getNext().getNext());
                break;
            }

            walker = walker.getNext();

        }
        return removed;
    }

    public void print() {
        boolean isFirst = true;
        Node walker = d_head;
        System.out.print("output: ");
        while (walker != null) {
            if (!isFirst) {
                System.out.print(",");
            } else {
                isFirst = false;
            }
            System.out.print(walker.getValue());
            walker = walker.getNext();
        }
        System.out.println();
    }

    public void partition(final int pivot) {
        boolean partitioned = false;
        Node walker_1 = d_head;
        Node walker_2 = d_head.getNext();
        while (!partitioned) {
            if (walker_2 == null || walker_1 == null) {
                break;
            }

            // advance the first iterator until we get to a value that can be swapped
            while (walker_1 != null && walker_1.getValue() < pivot) {
                walker_1 = walker_1.getNext();
            }

            walker_2 = walker_1.getNext();
            // advance the second iterator until we find a value that is smaller than the pivot
            // this value can be swapped with the value of walker_1
            while (walker_2 != null && walker_2.getValue() >= pivot) {
                walker_2 = walker_2.getNext();
            }

            if (walker_1 != null && walker_2 != null) {
                int temp = walker_1.getValue();
                walker_1.setValue(walker_2.getValue());
                walker_2.setValue(temp);
            }
        }
    }
}
