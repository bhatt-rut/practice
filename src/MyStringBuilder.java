import java.util.ArrayList;

/**
 * Created by rutvij on 4/2/16.
 */

public class MyStringBuilder {
    private ArrayList<String> d_internal;

    MyStringBuilder() {
        d_internal = new ArrayList<>();
    }

    public void append(String str) {
        d_internal.add(str);
    }

    public String toString() {
        // measure the total size of all the strings
        int totalSize = 0;
        for (String s : d_internal) {
            totalSize += s.length();
        }

        StringBuffer buf = new StringBuffer(totalSize);
        for (String s : d_internal) {
            buf.append(s);
        }

        return buf.toString();
    }
}
