/**
 * Created by rutvij on 4/3/16.
 */
public class Node {
    private Node d_next = null;
    private int  d_value;

    public Node(int x) {
        d_value = x;
    }

    public void setNext(Node next) {
        d_next = next;
    }

    public Node getNext() {
        return d_next;
    }

    public int getValue() {
        return d_value;
    }

    public void setValue(final int value) { d_value = value; }
}
